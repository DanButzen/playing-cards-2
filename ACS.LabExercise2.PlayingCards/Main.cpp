//Lab Exercise 2 - Playing Cards
//Tony Skoviak

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Rank
{
	Two=2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Diamonds, Hearts, Spades, Clubs
};

struct Card
{
	Rank cardRank;
	Suit cardSuit;
};

void PrintCard(const Card &card) { //print card rank + suit
	
	switch (card.cardRank) {
		case 14: cout << "The Ace";
			break;
		case 13: cout << "The King";
			break;
		case 12: cout << "The Queen";
			break;
		case 11: cout << "The Jack";
			break;
		case 10: cout << "The Ten";
			break;
		case 9: cout << "The Nine";
			break;
		case 8: cout << "The Eight";
			break;
		case 7: cout << "The Seven";
			break;
		case 6: cout << "The Six";
			break;
		case 5: cout << "The Five";
			break;
		case 4: cout << "The Four";
			break;
		case 3: cout << "The Three";
			break;
		case 2: cout << "The Two";
			break;
	}
	switch (card.cardSuit) {
		case 3: cout << " of Clubs";
			break;
		case 2: cout << " of Spades";
			break;
		case 1: cout << " of Hearts";
			break;
		case 0: cout << " of Diamonds";
			break;
	}

}

Card HighCard(const Card &card1, const Card &card2) {

	if (card1.cardRank > card2.cardRank) return card1; //compare ranks
	if (card1.cardRank < card2.cardRank) return card2;
	if (card1.cardSuit > card2.cardSuit) return card1; //compare suits
	if (card1.cardSuit < card1.cardSuit) return card2;
	return card1; //both cards are the same

}

int main() {
	Card card1;
	Card card2;

	card1.cardRank = Queen;
	card1.cardSuit = Diamonds;

	card2.cardRank = Ace;
	card2.cardSuit = Spades;

	PrintCard(card1);
	Card highCard = HighCard(card1, card2);
	cout << "\n";
	PrintCard(highCard);
	cout << " is higher.";
	_getch();
	return 0;
}